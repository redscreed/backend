<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSalaryRequest;
use App\Http\Requests\UpdateSalaryRequest;
use App\Http\Resources\SalaryItemResource;
use App\Models\Salary;
use Illuminate\Http\Response;

class SalaryController extends Controller
{
    /**
     * @param CreateSalaryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(CreateSalaryRequest $request)
    {
        try {
            $data = $request->toArray();
            $data['user_id'] = auth()->user()->id;

            Salary::create($data);

            return response()->json([], Response::HTTP_ACCEPTED, [], JSON_FORCE_OBJECT);
        } catch (\Exception $e){
            return response()->json([], Response::HTTP_BAD_REQUEST, [], JSON_FORCE_OBJECT);
        }

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $salary = Salary::findOrFail($id);

        return (new SalaryItemResource($salary))->response();
    }

    /**
     * @param $id
     * @param UpdateSalaryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, UpdateSalaryRequest $request)
    {
        try {
            $salary = Salary::findOrFail($id);

            $data = $request->toArray();

            $salary->update($data);
            return response()->json([], Response::HTTP_ACCEPTED, [], JSON_FORCE_OBJECT);
        } catch (\Exception $e) {
            return response()->json([], Response::HTTP_BAD_REQUEST, [], JSON_FORCE_OBJECT);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try {
            $salary = Salary::findOrFail($id);

            $salary->delete();
            return response()->json([], Response::HTTP_ACCEPTED, [], JSON_FORCE_OBJECT);
        } catch (\Exception $e) {
            return response()->json([], Response::HTTP_BAD_REQUEST, [], JSON_FORCE_OBJECT);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        $allCollection = Salary::all();

        return SalaryItemResource::collection($allCollection)->response();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function allByUser()
    {
        $userId = auth()->user()->id;

        $collection = Salary::where('user_id', $userId)->get();

        return SalaryItemResource::collection($collection)->response();
    }

}
