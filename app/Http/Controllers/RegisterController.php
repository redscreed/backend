<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{

    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        $data = $request->toArray();

        $data['password'] = Hash::make($data['password']);

        try {
            User::create($data);
            return response()->json([], Response::HTTP_ACCEPTED, [], JSON_FORCE_OBJECT);
        } catch (\Exception $e){
            return  response()->json([], Response::HTTP_BAD_REQUEST, [], JSON_FORCE_OBJECT);
        }
    }
}
