<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([

    'prefix' => 'salary',
    'middleware' => 'jwt.auth'

], function ($router) {
    Route::post('create', [\App\Http\Controllers\SalaryController::class, 'create']);
    Route::get('{id}/get', [\App\Http\Controllers\SalaryController::class, 'get']);
    Route::post('{id}/update', [\App\Http\Controllers\SalaryController::class, 'update']);
    Route::get('{id}/delete', [\App\Http\Controllers\SalaryController::class, 'delete']);
    Route::get('all', [\App\Http\Controllers\SalaryController::class, 'all']);
    Route::get('allByUser', [\App\Http\Controllers\SalaryController::class, 'allByUser']);
});

Route::group([

    'prefix'     => 'auth',

], function ($router) {
    Route::post('login', [\App\Http\Controllers\AuthController::class, 'login']);
    Route::post('logout', [\App\Http\Controllers\AuthController::class, 'logout']);
    Route::post('me', [\App\Http\Controllers\AuthController::class, 'me']);
    Route::post('register', [\App\Http\Controllers\RegisterController::class, 'register']);
});



